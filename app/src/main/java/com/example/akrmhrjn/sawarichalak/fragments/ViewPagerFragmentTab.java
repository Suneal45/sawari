package com.example.akrmhrjn.sawarichalak.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.example.akrmhrjn.sawarichalak.utils.AnkurTextView;
import com.example.akrmhrjn.sawarichalak.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;


@EFragment(R.layout.fragment_viewpager)
public class ViewPagerFragmentTab extends Fragment {

    @ViewById
    TextView tv_option1;

    @ViewById
    TextView tv_option2;

    @ViewById
    TextView tv_option3;

    @ViewById
    TextView tv_option4;

    @ViewById
    TextView tv_question;

    @ViewById
    AnkurTextView tv_option_no1;

    @ViewById
    AnkurTextView tv_option_no2;

    @ViewById
    AnkurTextView tv_option_no3;

    @ViewById
    AnkurTextView tv_option_no4;

    @ViewById
    LinearLayout ll_option_1;

    @ViewById
    LinearLayout ll_option_2;

    @ViewById
    LinearLayout ll_option_3;

    @ViewById
    LinearLayout ll_option_4;

    @ViewById
    TextView tv_no;

    @ViewById
    CoordinatorLayout rootLayout;

    @ViewById
    WebView WebView;

    Answers mCallback;
    Question question;

    String optionType;

    public static ViewPagerFragmentTab newInstance(Question question) {
        ViewPagerFragmentTab fragment = new ViewPagerFragmentTab_();
        Bundle args = new Bundle();
        args.putSerializable("question", question);
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    void init() {
        String text = "<html><body style=\"text-align:center; background:#1F77D0; color:#fff\"> %s </body></Html>";
        WebSettings settings = WebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        WebView.setBackgroundColor(Color.TRANSPARENT);

        optionType = Utils.optionType;

        System.out.println("=---=" + optionType);

        question = (Question) getArguments().getSerializable("question");

        /*Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"preeti.ttf");
        tv_question.setTypeface(font);*/

        if (question != null) {

            //WebView.loadData(String.format(text, question.question), "text/html; charset=utf-8", null);
            tv_no.setText(question.q_no);
            tv_question.setText(question.question);
            tv_option1.setText(question.option_1);
            tv_option2.setText(question.option_2);
            tv_option3.setText(question.option_3);
            tv_option4.setText(question.option_4);
            if (question.selectedOption != 0) {
                if (optionType.equalsIgnoreCase("exam")) {
                    selectOption(question.selectedOption);
                } else if (optionType.equalsIgnoreCase("practice")) {
                    if (getRightOption() == question.selectedOption)
                        selectRight(question.selectedOption);
                    else
                        selectRightWrong(getRightOption(), question.selectedOption);
                }
            }
        }
    }


    @Click(R.id.ll_option_1)
    void clickOption1() {
        mCallback.saveOptionSelection(1);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(1);
            if (question.answer.equalsIgnoreCase(getString(R.string.option1)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option1))) {
                //showRightToast();
                selectRight(1);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 1);
                mCallback.saveAnswer(0);
            }
        }
    }

    @Click(R.id.ll_option_2)
    void clickOption2() {
        mCallback.saveOptionSelection(2);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(2);
            if (question.answer.equalsIgnoreCase(getString(R.string.option2)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option2))) {
                //showRightToast();
                selectRight(2);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 2);
                mCallback.saveAnswer(0);
            }
        }
    }

    @Click(R.id.ll_option_3)
    void clickOption3() {
        mCallback.saveOptionSelection(3);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(3);
            if (question.answer.equalsIgnoreCase(getString(R.string.option3)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option3))) {
                //showRightToast();
                selectRight(3);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 3);
                mCallback.saveAnswer(0);
            }
        }
    }

    @Click(R.id.ll_option_4)
    void clickOption4() {
        mCallback.saveOptionSelection(4);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(4);
            if (question.answer.equalsIgnoreCase(getString(R.string.option4)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option4))) {
                //showRightToast();
                selectRight(4);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 4);
                mCallback.saveAnswer(0);
            }
        }
    }


    public void selectOption(int i) {
        LinearLayout linearLayout = getLinearLayout(i);
        clearLinearLayout();
        linearLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_pressed));
    }

    public void selectRightWrong(int right, int wrong) {
        LinearLayout Right = getLinearLayout(right);
        LinearLayout Wrong = getLinearLayout(wrong);
        AnkurTextView tvR = getTextView(right);
        AnkurTextView tvW = getTextView(wrong);
        clearLinearLayout();
        Right.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_right));
        Wrong.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_wrong));
        tvR.setText(getString(R.string.circle));
        tvR.setTextSize(18);
        tvR.setTextColor(getResources().getColor(R.color.greenAns));
        tvW.setText(getString(R.string.circle));
        tvW.setTextColor(getResources().getColor(R.color.redAns));
        tvW.setTextSize(18);
        disableClick();

    }

    public void selectRight(int i) {
        LinearLayout linearLayout = getLinearLayout(i);
        clearLinearLayout();
        AnkurTextView tvR = getTextView(i);
        tvR.setText(getString(R.string.circle));
        tvR.setTextSize(18);
        tvR.setTextColor(getResources().getColor(R.color.greenAns));
        linearLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_right));
        disableClick();
    }

    public void clearTextViews() {
        tv_option1.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
        tv_option2.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
        tv_option3.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
        tv_option4.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
    }

    public void clearLinearLayout() {
        ll_option_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
        ll_option_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
        ll_option_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
        ll_option_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                mCallback = (Answers) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement Answers");
            }
        }
    }

    public interface Answers {
        void saveAnswer(int value);

        void saveOptionSelection(int option);
    }

    public AnkurTextView getTextView(int option) {
        switch (option) {
            case 1:
                return tv_option_no1;
            case 2:
                return tv_option_no2;
            case 3:
                return tv_option_no3;
            case 4:
                return tv_option_no4;
            default:
                return null;
        }
    }

    public LinearLayout getLinearLayout(int option) {
        switch (option) {
            case 1:
                return ll_option_1;
            case 2:
                return ll_option_2;
            case 3:
                return ll_option_3;
            case 4:
                return ll_option_4;
            default:
                return null;
        }
    }

    void disableClick() {
        ll_option_1.setClickable(false);
        ll_option_2.setClickable(false);
        ll_option_3.setClickable(false);
        ll_option_4.setClickable(false);
    }

    public int getRightOption() {
        if (question.answer.equalsIgnoreCase(getString(R.string.option1)))
            return 1;
        else if (question.answer.equalsIgnoreCase(getString(R.string.option2)))
            return 2;
        else if (question.answer.equalsIgnoreCase(getString(R.string.option3)))
            return 3;
        else
            return 4;
    }
}