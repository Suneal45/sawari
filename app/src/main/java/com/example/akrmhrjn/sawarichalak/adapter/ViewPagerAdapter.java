package com.example.akrmhrjn.sawarichalak.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.akrmhrjn.sawarichalak.fragments.PicViewPagerFragmentTab;
import com.example.akrmhrjn.sawarichalak.fragments.SelectiveViewPagerFragmentTab;
import com.example.akrmhrjn.sawarichalak.fragments.ViewPagerFragmentTab;
import com.example.akrmhrjn.sawarichalak.model.Question;

import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    List<Question> questionList;
    String type;

    public ViewPagerAdapter(FragmentManager fm, List<Question> questionList, String type) {
        super(fm);
        this.questionList = questionList;
        this.type = type;
    }

    @Override
    public int getCount() {
        if (questionList != null)
            return questionList.size();
        else return 0;
    }

    @Override
    public Fragment getItem(int position) {

        if (type.equalsIgnoreCase("mcq"))
            return ViewPagerFragmentTab.newInstance(questionList.get(position));
        else if (type.equalsIgnoreCase("pic"))
            return PicViewPagerFragmentTab.newInstance(questionList.get(position));
        else if (type.equalsIgnoreCase("sq"))
            return SelectiveViewPagerFragmentTab.newInstance(questionList.get(position));
        else if (type.equalsIgnoreCase("misq")) {
            if (position < 2) {
                return SelectiveViewPagerFragmentTab.newInstance(questionList.get(position));
            } else {
                return ViewPagerFragmentTab.newInstance(questionList.get(position));
            }
        } else
            return null;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return questionList.get(position).q_no.toUpperCase();
    }
}
