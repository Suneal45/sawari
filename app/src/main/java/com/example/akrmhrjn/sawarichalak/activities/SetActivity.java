package com.example.akrmhrjn.sawarichalak.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.adapter.RvSetsAdapter;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.example.akrmhrjn.sawarichalak.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;


@EActivity(R.layout.activity_sets)
public class SetActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    RecyclerView recyclerView;

    LinearLayoutManager mLayoutManager;
    RvSetsAdapter mAdapter;

    @Extra
    String title;

    @AfterViews
    void init() {
        initToolbar();

        List<String> sets = new ArrayList<String>();
        if (Utils.setType.equalsIgnoreCase("SetB")) {
            //set b
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set", "mcq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        } else if (Utils.setType.equalsIgnoreCase("trafficSigns")) {
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set", "pq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        } else if (Utils.setType.equalsIgnoreCase("SetA")) {
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set", "sq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        } else {
            //unknown case
        }

        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        // specify an adapter
        mAdapter = new RvSetsAdapter(this, sets);
        recyclerView.setAdapter(mAdapter);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OptionsItem(android.R.id.home)
    void back() {
        onBackPressed();
    }
}
