package com.example.akrmhrjn.sawarichalak.activities;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.User;
import com.example.akrmhrjn.sawarichalak.retrofit.RestClient;
import com.example.akrmhrjn.sawarichalak.retrofit.RetroAPIService;
import com.example.akrmhrjn.sawarichalak.utils.Utils;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 2/27/16.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {
    SimpleFacebook mSimpleFacebook;
    Permission[] permissions = new Permission[]{
            Permission.USER_PHOTOS,
            Permission.EMAIL,
            Permission.PUBLISH_ACTION
    };
    Context context = this;

    @AfterViews
    void init() {
        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(this.getResources().getString(R.string.app_id))
                .setNamespace(this.getResources().getString(R.string.app_namespace))
                .setPermissions(permissions)
                .build();
        SimpleFacebook.setConfiguration(configuration);

    }

    @Click(R.id.login_button)
    void loginFacebook() {

        mSimpleFacebook = SimpleFacebook.getInstance(this);
        OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // change the state of the button or do whatever you want
                Log.i("Facebook", "Logged in");
            }

            @Override
            public void onCancel() {
                Log.i("Facebook", "Cancelled");
                // user canceled the dialog
            }

            @Override
            public void onFail(String reason) {
                Log.i("Facebook", "Fail");
                // failed to login
            }

            @Override
            public void onException(Throwable throwable) {
                Log.i("Facebook", "Expections");
                // exception from facebook
            }

        };
        mSimpleFacebook.login(onLoginListener);

        Profile.Properties properties = new Profile.Properties.Builder()
                .add(Profile.Properties.ID)
                .add(Profile.Properties.FIRST_NAME)
                .add(Profile.Properties.LAST_NAME)
                .add(Profile.Properties.PICTURE)
                .build();

        OnProfileListener onProfileListener = new OnProfileListener() {
            @Override
            public void onComplete(Profile profile) {
                String currentUserID = PreferenceManager.getDefaultSharedPreferences(context).getString("user_id", "");
                String currentFirstName = PreferenceManager.getDefaultSharedPreferences(context).getString("first_name", "");
                String currentLastName = PreferenceManager.getDefaultSharedPreferences(context).getString("last_name", "");
                String currentImage = PreferenceManager.getDefaultSharedPreferences(context).getString("image", "");

                String fbUserId = profile.getId();
                String fbFirstName = profile.getFirstName();
                String fbLastName = profile.getLastName();
                String fbImage = profile.getPicture();

                if (!currentUserID.equalsIgnoreCase("")) {
                    if (!currentUserID.equalsIgnoreCase(fbUserId) || !currentFirstName.equalsIgnoreCase(fbFirstName) ||
                            !currentLastName.equalsIgnoreCase(fbLastName) || !currentImage.equalsIgnoreCase(fbImage)) {
                        saveToSharedPreference(fbUserId, fbFirstName, fbLastName, fbImage);
                        //TODO UPDATE
                        //saveLoginInformation(fbUserId, fbFirstName, fbLastName, fbImage);

                    }
                } else {
                    saveToSharedPreference(fbUserId, fbFirstName, fbLastName, fbImage);
                    saveLoginInformation(fbUserId, fbFirstName, fbLastName, fbImage);
                }
            }

    /*
     * You can override other methods here:
     * onThinking(), onFail(String reason), onException(Throwable throwable)
     */
        };
        mSimpleFacebook.getProfile(properties, onProfileListener);

    }


    @Background
    void saveLoginInformation(final String userId, String firstName, String lastName, final String image) {
        RestClient rsClient = new RestClient();
        RetroAPIService restAPI = rsClient.getApiService();
        User user = new User(userId, firstName, lastName, image);

        restAPI.createUser(user, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                System.out.println("User saved to server DB" + response.toString());
                AfterSuccess(userId, image);
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Failure saving User" + error.toString());
                //TODO send to MAINACTIVITY?
            }
        });
    }

    @UiThread
    void AfterSuccess(String userId, String image) {
        Utils.saveProfilePicToCache(context, image, userId);
        MainActivity_.intent(LoginActivity.this).start();
        finish();
    }

    protected void saveToSharedPreference(String id, String firstName, String lastName, String image) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("first_name", firstName).commit();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("last_name", lastName).commit();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("user_id", id).commit();
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("image", image).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
