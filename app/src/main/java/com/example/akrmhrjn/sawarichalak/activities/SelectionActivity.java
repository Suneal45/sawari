package com.example.akrmhrjn.sawarichalak.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_selector)
public class SelectionActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @Extra
    String from;

    String title;

    @AfterViews
    void init() {
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OptionsItem(android.R.id.home)
    void back() {
        onBackPressed();
    }

    @Click(R.id.btnsetA)
    void openPracticeSets() {
        Utils.setType = "SetA";
        title = getString(R.string.setA);
        SetActivity_.intent(this).extra("title", title).start();
    }

    @Click(R.id.btnSetB)
    void openSetB() {
        title = "setB";
        Utils.setType = "SetB";
        Utils.optionType = from;
        SetActivity_.intent(this).extra("title", title).start();
    }

    @Click(R.id.btnPictureSet)
    void openPictureSets() {
        title = "trafficSigns";
        Utils.setType = "trafficSigns";
        Utils.optionType = from;
        SetActivity_.intent(this).extra("title", title).start();
    }
}
