package com.example.akrmhrjn.sawarichalak.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.adapter.ViewPagerAdapter;
import com.example.akrmhrjn.sawarichalak.fragments.PicViewPagerFragmentTab;
import com.example.akrmhrjn.sawarichalak.fragments.SelectiveAnswerFragment;
import com.example.akrmhrjn.sawarichalak.fragments.ViewPagerFragmentTab;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.example.akrmhrjn.sawarichalak.utils.NepaliTextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;


@EActivity(R.layout.activity_viewpager)
public class ViewPagerActivity extends AppCompatActivity implements ViewPagerFragmentTab.Answers, SelectiveAnswerFragment.Answers, PicViewPagerFragmentTab.Answers{

    @ViewById
    ViewPager pager;

    @ViewById
    Toolbar toolbar;

    @ViewById
    NepaliTextView pageNumber;

    @ViewById
    Button btnNext;

    @ViewById
    Button btnPrevious;

    @ViewById
    Button btnSubmit;

    @ViewById
    NepaliTextView q_no;

    @Extra
    String type; //mcq or sq or pic question types

    @Extra
    int position;

    List<Question> questionList;

    @AfterViews
    void init() {

        initToolbar();

        questionList = new ArrayList<Question>();
        switch (type) {
            case "sq":
                questionList = getSQQuestions();
                break;
            case "mcq":
                questionList = getMCQQuestions();
                break;
            case "pic":
                questionList = getPQQuestions();
                break;
            case "misq":
                questionList = getMiscQuestions();
                break;
            default:
                break;

        }

        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), questionList, type);
        pager.setAdapter(adapter);

        pageNumber.setText(getString(R.string.page)+ " " + (Integer.valueOf(pager.getCurrentItem()) + 1) + " / " + questionList.size());
        if (pager.getCurrentItem() == 0) {
            btnPrevious.setVisibility(View.INVISIBLE);
        } else {
            btnPrevious.setVisibility(View.VISIBLE);
        }

        final List<Question> finalQuestionList = questionList;
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                pageNumber.setText(getString(R.string.page)+ " " + (i + 1) + " / " + finalQuestionList.size());
                q_no.setText(String.valueOf((i + 1)));
                if (i == 0) {
                    btnPrevious.setVisibility(View.INVISIBLE);
                } else {
                    btnPrevious.setVisibility(View.VISIBLE);
                }

                if (i == finalQuestionList.size() - 1) {
                    btnSubmit.setVisibility(View.VISIBLE);
                    btnNext.setVisibility(View.INVISIBLE);
                } else {
                    btnSubmit.setVisibility(View.INVISIBLE);
                    btnNext.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    List<Question> getMCQQuestions() {
        List<Question> questionList = Question.find(Question.class, "question_set LIKE ?", "mcq_" + position);
        return questionList;
    }

    List<Question> getSQQuestions() {
        List<Question> questionList = Question.find(Question.class, "question_set LIKE ?", "sq_" + position);
        return questionList;
    }

    List<Question> getPQQuestions() {
        List<Question> questionList = Question.find(Question.class, "question_set LIKE ?", "pq_" + position);
        return questionList;
    }
    List<Question> getMiscQuestions() {
        List<Question> squestions = Question.find(Question.class, "question_set LIKE ? limit 2", "sq_"+position);
        List<Question> mcquestions = Question.find(Question.class, "question_set LIKE ? limit 18", "mcq_"+position);
        squestions.addAll(mcquestions);
        return squestions;

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));

        if(type.equalsIgnoreCase("sq"))
            getSupportActionBar().setTitle(getString(R.string.setA));
        else if(type.equalsIgnoreCase("mcq"))
            getSupportActionBar().setTitle(getString(R.string.setB));
        else if(type.equalsIgnoreCase("pic"))
            getSupportActionBar().setTitle(getString(R.string.traffic_signs));
        else if (type.equalsIgnoreCase("misq"))
            getSupportActionBar().setTitle(getString(R.string.practise_sets));
    }

    @Click(R.id.btnNext)
    void Next() {
        pager.setCurrentItem(pager.getCurrentItem() + 1, true);
    }

    @Click(R.id.btnPrevious)
    void Previous() {
        pager.setCurrentItem(pager.getCurrentItem() - 1, true);
    }

    @Click(R.id.btnSubmit)
    void Submit() {
        int score = 0;
        for (Question question : questionList) {
            score = score + question.point;
        }
        final MaterialDialog mMaterialDialog = new MaterialDialog(ViewPagerActivity.this);
        mMaterialDialog.setTitle("Your Score")
                .setMessage(score + " out of " + questionList.size())
                .setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        onBackPressed();
                    }
                })
                .show();
    }

    @OptionsItem(android.R.id.home)
    void back() {
        onBackPressed();
    }

    @Override
    public void saveAnswer(int value) {
        questionList.get(pager.getCurrentItem()).point = value;
    }


    @Override
    public void saveOptionSelection(int option) {
        questionList.get(pager.getCurrentItem()).selectedOption = option;
    }

   /* @Override
    public void nextPage() {
        Next();
    }*/
}
