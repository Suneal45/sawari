package com.example.akrmhrjn.sawarichalak.model;

import com.orm.SugarRecord;

import java.io.Serializable;


public class Question extends SugarRecord<Question> implements Serializable {

    public String q_no;
    public String question;
    public String option_1;
    public String option_2;
    public String option_3;
    public String option_4;
    public String answer;
    public String answer_string;
    public int selectedOption;
    public int point;
    public String questionSet;

}
