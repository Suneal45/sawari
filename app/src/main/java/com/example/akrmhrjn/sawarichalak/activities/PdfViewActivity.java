package com.example.akrmhrjn.sawarichalak.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.akrmhrjn.sawarichalak.R;
import com.joanzapata.pdfview.PDFView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_pdfview)
public class PdfViewActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    PDFView pdfView;

    @Extra
    String pdf;


    @AfterViews
    void init() {
        initToolbar();
      pdfView.fromAsset(pdf)
               .defaultPage(1)
                .showMinimap(false)
                .enableSwipe(true)
              .swipeVertical(true)
                .load();


    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
    }

    @OptionsItem(android.R.id.home)
    void back() {
        onBackPressed();
    }
}
