package com.example.akrmhrjn.sawarichalak.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;

public class NepaliTextView extends TextView {


    public NepaliTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Typeface font = Typeface.createFromAsset(context.getAssets(), "nepali.ttf");
        setTypeface(font);
    }
}
