package com.example.akrmhrjn.sawarichalak.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.adapter.RvSetsAdapter;
import com.example.akrmhrjn.sawarichalak.model.Question;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/13/15.
 */
@EActivity(R.layout.activity_question_sets)
public class QueSetsActivity extends AppCompatActivity {
    @ViewById
    RecyclerView recyclerView;

    @ViewById
    Toolbar toolbar;


    LinearLayoutManager mLayoutManager;
    RvSetsAdapter mAdapter;

    @Extra
    String title;

    @AfterViews
    void init() {
        initToolbar();
        List<String> sets = new ArrayList<String>();
        if (title.equalsIgnoreCase(getString(R.string.setA))) {
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set limit 10", "sq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        }else if (title.equalsIgnoreCase(getString(R.string.practise_sets))){
            sets.add("misq_1"); //TODO esari naming diney ra?
        }

        else {
            //picture case
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set", "pq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        }
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        mAdapter = new RvSetsAdapter(this, sets);
        recyclerView.setAdapter(mAdapter);
    }
    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));

        if(title.equalsIgnoreCase(getString(R.string.setA)))
            getSupportActionBar().setTitle(getString(R.string.setA));
        else if(title.equalsIgnoreCase(getString(R.string.practise_sets)))
            getSupportActionBar().setTitle(getString(R.string.practise_sets));
    }

    @OptionsItem(android.R.id.home)
    void back() {
        onBackPressed();
    }
}
