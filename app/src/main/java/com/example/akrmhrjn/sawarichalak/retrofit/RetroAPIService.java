package com.example.akrmhrjn.sawarichalak.retrofit;

import com.example.akrmhrjn.sawarichalak.model.QuestionSets;
import com.example.akrmhrjn.sawarichalak.model.User;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;


public interface RetroAPIService {
    @GET("/Suneal/c73bd7093fe396fddc44/raw/950fc38ccd5984aba883934f006dda0a87e96f14/")
    QuestionSets getQuestions();
    @POST("/sawarichalakapi/public/api/createuser")
    void createUser(@Body User user, Callback<User> cb);
}
