package com.example.akrmhrjn.sawarichalak.fragments;

import android.support.v4.app.Fragment;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.activities.PdfViewActivity_;
import com.example.akrmhrjn.sawarichalak.activities.QueSetsActivity_;
import com.example.akrmhrjn.sawarichalak.activities.SelectionActivity_;
import com.example.akrmhrjn.sawarichalak.activities.VPagerQueSetsActivity_;
import com.example.akrmhrjn.sawarichalak.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_dashboard)
public class DashboardFragment extends Fragment {

    String title;
    @AfterViews
    void init() {

    }

    @Click(R.id.btnLearn)
    void openLearnSection() {
        SelectionActivity_.intent(this).extra("from", "practice").start();
    }

    @Click(R.id.btnExam)
    void openExamSection() {
        SelectionActivity_.intent(this).extra("from", "exam").start();
    }

    @Click(R.id.btnsetA)
    void openPracticeSets() {
        title = getString(R.string.setA);
        QueSetsActivity_.intent(this).extra("title",title).start();
    }

    @Click(R.id.btnSetB)
    void openSetB() {
        title = "SetB";
        Utils.setType = "SetB";
        Utils.optionType = "practice";
        VPagerQueSetsActivity_.intent(this).extra("title",title).start();
    }

    @Click(R.id.btnPictureSet)
    void openPictureSets() {
        title = "trafficSigns";
        Utils.setType = "trafficSigns";
        Utils.optionType = "practice";
        VPagerQueSetsActivity_.intent(this).extra("title", title).start();
    }

    @Click(R.id.btnPractiseMisc)
    void openMiscSet(){
        title = getString(R.string.practise_sets);
        QueSetsActivity_.intent(this).extra("title",title).start();
    }

    @Click(R.id.btnInfo)
    void openInfo(){
        //title = getString(R.string.practise_sets);
         PdfViewActivity_.intent(this).extra("pdf","information.pdf").start();
    }
    @Click(R.id.btnModelSet)
    void openModelSet() {
        PdfViewActivity_.intent(this).extra("pdf","model.pdf").start();
    }

}
