package com.example.akrmhrjn.sawarichalak.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;


public class RestClient {

    //private static final String BASE_URL = "http://akrmhrjn.byethost32.com";
    private static final String BASE_URL = "http://makuracreations.com";
    private RetroAPIService apiService;

    public RestClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        apiService = restAdapter.create(RetroAPIService.class);
    }

    public RetroAPIService getApiService() {
        return apiService;
    }

}
