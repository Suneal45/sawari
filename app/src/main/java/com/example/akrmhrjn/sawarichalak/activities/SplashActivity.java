package com.example.akrmhrjn.sawarichalak.activities;

import android.content.Context;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.MCQ;
import com.example.akrmhrjn.sawarichalak.model.PQ;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.example.akrmhrjn.sawarichalak.model.QuestionSets;
import com.example.akrmhrjn.sawarichalak.model.SQ;
import com.example.akrmhrjn.sawarichalak.retrofit.RestClient;
import com.example.akrmhrjn.sawarichalak.retrofit.RetroAPIService;
import com.example.akrmhrjn.sawarichalak.utils.NetworkStatus;
import com.sromku.simple.fb.SimpleFacebook;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    String userId;
    SimpleFacebook mSimpleFacebook;

    private static final int SPLASH_DURATION = 3000; //3 seconds
    final Context context = this;

    @ViewById
    ProgressBar progressBar;

    @ViewById
    TextView downloadingImg;

    @AfterViews
    void init() {
        userId= PreferenceManager.getDefaultSharedPreferences(this).getString("user_id", "");

        List<Question> questionList = Question.listAll(Question.class);

        if (NetworkStatus.isConnected(context) && (questionList == null || questionList.isEmpty())) {
            Toast.makeText(context, "Updating from server!!!", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.VISIBLE);
            getQuestions();
        } else {
            Handler mHandler = new Handler();

            // run a thread to start the home screen
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    if (NetworkStatus.isConnected(context) && userId =="") {
                       LoginActivity_.intent(context).start();
                       finish();
                    } else {
                        MainActivity_.intent(context).start();
                        finish();
                    }
                }
            }, SPLASH_DURATION);
        }
    }

    @Background
    void getQuestions() {
        RestClient rsClient = new RestClient();
        RetroAPIService restAPI = rsClient.getApiService();
        QuestionSets questionsSets = restAPI.getQuestions();
        AfterGetQuestionSets(questionsSets);
    }

    @UiThread
    void AfterGetQuestionSets(QuestionSets questionSets) {
        if (questionSets == null) return;
        for (int i = 0; i < questionSets.sq_set.size(); i++){
            SQ sq = questionSets.sq_set.get(i);
            for(Question question: sq.sq){
                question.q_no = String.valueOf(i + 1);
                question.option_1 = "";
                question.option_2 = "";
                question.option_3 = "";
                question.option_4 = "";
                question.answer_string = "";
                question.questionSet = "sq_"+i;
                question.save();
            }
        }

        for (int i = 0; i < questionSets.mcq_set.size(); i++){
            MCQ mcq = questionSets.mcq_set.get(i);
            for(Question question: mcq.mcq){
                question.answer_string = "";
                question.questionSet = "mcq_"+i;
                question.save();
            }
        }

        for (int i = 0; i < questionSets.pq_set.size(); i++){
            PQ pq = questionSets.pq_set.get(i);
            for(Question question : pq.pq) {
                //Utils.saveToCache(SplashActivity.this, question.answer_string);
                question.questionSet = "pq_"+i;
                question.save();
            }
        }
        LoginActivity_.intent(context).start();
        finish();
    }
    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }
}