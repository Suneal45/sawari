package com.example.akrmhrjn.sawarichalak.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.Question;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

/**
 * Created by user on 10/3/15.
 */
@EFragment(R.layout.fragment_viewpager_selective)
public class SelectiveViewPagerFragmentTab extends Fragment implements SelectiveQuestionFragment.ShowAnswer {
    private int optionSelected = 1;

    private Fragment questionFragment;
    private Fragment answerFragment;
    Question question;


    public static SelectiveViewPagerFragmentTab newInstance(Question question) {
        SelectiveViewPagerFragmentTab fragment = new SelectiveViewPagerFragmentTab_();
        Bundle args = new Bundle();
        args.putSerializable("question", question);
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    void init() {
        question = (Question) getArguments().getSerializable("question");

        //Add first fragment
        questionFragment = new SelectiveQuestionFragment_();
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.container, questionFragment);

        Bundle args = new Bundle();
        args.putSerializable("question", question);
        questionFragment.setArguments(args);
        fragmentTransaction.commit();
    }

    @Override
    public void showClickedAnswer() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            answerFragment = new SelectiveAnswerFragment_();
            FragmentManager fm = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            //fragmentTransaction.setCustomAnimations(R.anim.anim_in, R.anim.anim_out);
            fragmentTransaction.replace(R.id.container, answerFragment);
            Bundle args = new Bundle();
            args.putSerializable("question", question);
            answerFragment.setArguments(args);
            fragmentTransaction.commit();
        } else {
            getFragmentManager().popBackStack();
        }
    }


}
