package com.example.akrmhrjn.sawarichalak.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.adapter.VpagerQueSetsAdapter;
import com.example.akrmhrjn.sawarichalak.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;



@EActivity(R.layout.activity_vpager_qsets)
public class VPagerQueSetsActivity extends AppCompatActivity{

    @ViewById
    Toolbar toolbar;

    @ViewById
    TabLayout tabLayout;

    @ViewById
    ViewPager pager;

    @Extra
    String title;

    String[] pagerTitles;


    @AfterViews
    void init(){
        initToolbar();
        pagerTitles = new String[]{getString(R.string.practice), getString(R.string.exam)};
        setupViewPager(pager);
        tabLayout.setupWithViewPager(pager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        System.out.println("+++Practice");
                        Utils.optionType = "practice";
                        break;
                    case 1:
                        System.out.println("+++Exam");
                        Utils.optionType = "exam";
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        VpagerQueSetsAdapter adapter = new VpagerQueSetsAdapter(getSupportFragmentManager(), pagerTitles);
        viewPager.setAdapter(adapter);

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(title.equalsIgnoreCase("SetB"))
            getSupportActionBar().setTitle(getString(R.string.setB));
        else if(title.equalsIgnoreCase("trafficSigns"))
            getSupportActionBar().setTitle(getString(R.string.traffic_signs));
    }

    @OptionsItem(android.R.id.home)
    void back() {
        onBackPressed();
    }
}
