package com.example.akrmhrjn.sawarichalak.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.adapter.RvSetsAdapter;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.example.akrmhrjn.sawarichalak.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;



@EFragment(R.layout.fragment_question_sets)
public class QuestionSetsFragment extends Fragment {

    @ViewById
    RecyclerView recyclerView;

    LinearLayoutManager mLayoutManager;
    RvSetsAdapter mAdapter;

    String title;

    public static QuestionSetsFragment newInstance(String title) {
        QuestionSetsFragment fragment = new QuestionSetsFragment_();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    void init() {

        title = getArguments().getString("title");
        List<String> sets = new ArrayList<String>();
        if (Utils.setType.equalsIgnoreCase("setB")) {
            //set b
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set", "mcq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        } else if(Utils.setType.equalsIgnoreCase("trafficSigns")) {
            List<Question> questions = Question.findWithQuery(Question.class, "Select * from Question where question_set LIKE ? group by question_set", "pq%");
            for (Question question : questions) {
                sets.add(question.questionSet);
            }
        } else{
            //unknown case
        }

        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        mAdapter = new RvSetsAdapter(getActivity(), sets);
        recyclerView.setAdapter(mAdapter);
    }

}
