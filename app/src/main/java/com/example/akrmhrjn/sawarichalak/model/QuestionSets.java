package com.example.akrmhrjn.sawarichalak.model;

import java.io.Serializable;
import java.util.List;

public class QuestionSets implements Serializable{

    public List<MCQ> mcq_set;
    public List<SQ> sq_set;
    public List<PQ> pq_set;

}