package com.example.akrmhrjn.sawarichalak.utils;


import android.content.Context;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.download.ImageDownloader;
import com.nostra13.universalimageloader.utils.IoUtils;
import com.nostra13.universalimageloader.utils.L;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class Utils {
    public static String optionType = ""; //to distinguish practice or exam
    public static String setType = ""; //to distinguish picture or set B


    public static boolean saveToCache(Context context, String picName) {
        String imageUrl = "http://103.253.146.215:3000/assets/Pratibandhaatmak/" + picName + ".png";
        int i = imageUrl.length() - 1;
        while (imageUrl.charAt(i) != '/') {
            i--;
        }
        String newImageUrl = imageUrl.substring(i + 1);

        File directory = new File(context.getCacheDir(), "sawariChalak");
        if (!directory.exists()) {
            directory.mkdir();
        }


        File fileForImage = new File(directory, newImageUrl);

        boolean saved = false;

        InputStream sourceStream = null;
        try {

            ImageDownloader downloader = new BaseImageDownloader(context);
            sourceStream = downloader.getStream(imageUrl, null);
        } catch (IOException e) {
            L.e(e);
        }

        if (sourceStream != null) {
            try {
                System.out.println(fileForImage.getAbsolutePath());
                OutputStream targetStream = new FileOutputStream(fileForImage);
                IoUtils.copyStream(sourceStream, targetStream, null);
                targetStream.close();
                sourceStream.close();
                saved = true;
            } catch (IOException e) {
                L.e(e);
            }
        }

        return saved;

    }

    public static boolean saveProfilePicToCache(Context context, String imageUrl, String profileId) {
        int i = imageUrl.length() - 1;
        while (imageUrl.charAt(i) != '/') {
            i--;
        }

        File directory = new File(context.getCacheDir(), "sawariChalak");
        if (!directory.exists()) {
            directory.mkdir();
        }


        File fileForImage = new File(directory, profileId+".jpg");

        boolean saved = false;

        InputStream sourceStream = null;
        try {

            ImageDownloader downloader = new BaseImageDownloader(context);
            sourceStream = downloader.getStream(imageUrl, null);
        } catch (IOException e) {
            L.e(e);
        }

        if (sourceStream != null) {
            try {
                System.out.println(fileForImage.getAbsolutePath());
                OutputStream targetStream = new FileOutputStream(fileForImage);
                IoUtils.copyStream(sourceStream, targetStream, null);
                targetStream.close();
                sourceStream.close();
                saved = true;
            } catch (IOException e) {
                L.e(e);
            }
        }

        return saved;

    }
}
