package com.example.akrmhrjn.sawarichalak.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.example.akrmhrjn.sawarichalak.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


@EFragment(R.layout.fragment_viewpager_pic)
public class PicViewPagerFragmentTab extends Fragment {

    @ViewById
    LinearLayout box_option1;

    @ViewById
    LinearLayout box_option2;

    @ViewById
    LinearLayout box_option3;

    @ViewById
    LinearLayout box_option4;

    @ViewById
    TextView tv_question;

    @ViewById
    TextView tv_no;

    @ViewById
    CoordinatorLayout rootLayout;

    @ViewById
    ImageView img_option1;

    @ViewById
    ImageView img_option2;

    @ViewById
    ImageView img_option3;

    @ViewById
    ImageView img_option4;



    Answers mCallback;
    Question question;
    String optionType;

    public static PicViewPagerFragmentTab newInstance(Question question) {
        PicViewPagerFragmentTab fragment = new PicViewPagerFragmentTab_();
        Bundle args = new Bundle();
        args.putSerializable("question", question);
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    void init() {
        optionType = Utils.optionType;

        String imgPath = "file://"+getActivity().getCacheDir()+"/sawariChalak/";

        question = (Question) getArguments().getSerializable("question");
        if (question != null) {
            tv_question.setText(question.question);
            tv_no.setText(question.q_no);

            try{
                ImageLoader.getInstance().displayImage(imgPath + question.option_1 + ".png", img_option1);
                ImageLoader.getInstance().displayImage(imgPath + question.option_2 + ".png", img_option2);
                ImageLoader.getInstance().displayImage(imgPath + question.option_3 + ".png", img_option3);
                ImageLoader.getInstance().displayImage(imgPath + question.option_4 + ".png", img_option4);
            }catch (Exception err){
                err.printStackTrace();
            }

        }

        if (question.selectedOption != 0) {
            if (optionType.equalsIgnoreCase("exam")) {
                selectOption(question.selectedOption);
            } else if (optionType.equalsIgnoreCase("practice")) {
                if (getRightOption() == question.selectedOption)
                    selectRight(question.selectedOption);
                else
                    selectRightWrong(getRightOption(), question.selectedOption);
            }
        }


    }

    @Click(R.id.box_option1)
    void clickOption1() {
        mCallback.saveOptionSelection(1);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(1);
            if (question.answer.equalsIgnoreCase(getString(R.string.option1)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option1))) {
                //showRightToast();
                selectRight(1);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 1);
                mCallback.saveAnswer(0);
            }
        }
    }

    @Click(R.id.box_option2)
    void clickOption2() {
        mCallback.saveOptionSelection(2);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(2);
            if (question.answer.equalsIgnoreCase(getString(R.string.option2)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option2))) {
                //showRightToast();
                selectRight(2);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 2);
                mCallback.saveAnswer(0);
            }
        }
    }

    @Click(R.id.box_option3)
    void clickOption3() {
        mCallback.saveOptionSelection(3);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(3);
            if (question.answer.equalsIgnoreCase(getString(R.string.option3)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option3))) {
                //showRightToast();
                selectRight(3);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 3);
                mCallback.saveAnswer(0);
            }
        }
    }

    @Click(R.id.box_option4)
    void clickOption4() {
        mCallback.saveOptionSelection(4);
        if (optionType.equalsIgnoreCase("exam")) {
            selectOption(4);
            if (question.answer.equalsIgnoreCase(getString(R.string.option4)))
                mCallback.saveAnswer(1);
            else
                mCallback.saveAnswer(0);
        } else if (optionType.equalsIgnoreCase("practice")) {
            if (question.answer.equalsIgnoreCase(getString(R.string.option4))) {
                //showRightToast();
                selectRight(4);
                mCallback.saveAnswer(1);
            } else {
                //showWrongToast();
                selectRightWrong(getRightOption(), 4);
                mCallback.saveAnswer(0);
            }
        }
    }


    public void selectOption(int i) {
        LinearLayout linearLayout = getLinearLayout(i);
        clearLinearLayouts();
        linearLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_pressed));
    }

    public void selectRightWrong(int right, int wrong) {
        LinearLayout RightLinearLayout = getLinearLayout(right);
        LinearLayout WrongLinearLayout = getLinearLayout(wrong);
        clearLinearLayouts();
        RightLinearLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_right));
        WrongLinearLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_wrong));
        disableClick();

    }

    public void selectRight(int i) {
        LinearLayout linearLayout = getLinearLayout(i);
        clearLinearLayouts();
        linearLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.tv_answer_right));
        disableClick();
    }

    public void clearLinearLayouts() {
        box_option1.setBackgroundDrawable(getResources().getDrawable(R.drawable.pic_answer_box));
        box_option2.setBackgroundDrawable(getResources().getDrawable(R.drawable.pic_answer_box));
        box_option3.setBackgroundDrawable(getResources().getDrawable(R.drawable.pic_answer_box));
        box_option4.setBackgroundDrawable(getResources().getDrawable(R.drawable.pic_answer_box));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                mCallback = (Answers) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement Answers");
            }
        }
    }

    public interface Answers {
        void saveAnswer(int value);

        void saveOptionSelection(int option);
    }

    public LinearLayout getLinearLayout(int option) {
        switch (option) {
            case 1:
                return box_option1;
            case 2:
                return box_option2;
            case 3:
                return box_option3;
            case 4:
                return box_option4;
            default:
                return null;
        }
    }

    void disableClick() {
        box_option1.setClickable(false);
        box_option2.setClickable(false);
        box_option3.setClickable(false);
        box_option4.setClickable(false);
    }

    public int getRightOption() {
        if (question.answer.equalsIgnoreCase(getString(R.string.option1)))
            return 1;
        else if (question.answer.equalsIgnoreCase(getString(R.string.option2)))
            return 2;
        else if (question.answer.equalsIgnoreCase(getString(R.string.option3)))
            return 3;
        else
            return 4;
    }

    /*void showRightToast() {
        Crouton.cancelAllCroutons();
        Crouton.makeText(getActivity(), getString(R.string.right), Style.CONFIRM, rootLayout).show();
    }

    void showWrongToast() {
        Crouton.cancelAllCroutons();
        Crouton.makeText(getActivity(), getString(R.string.wrong), Style.ALERT, rootLayout).show();
    }*/

}