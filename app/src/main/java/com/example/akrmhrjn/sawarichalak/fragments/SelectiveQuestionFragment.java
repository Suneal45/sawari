package com.example.akrmhrjn.sawarichalak.fragments;


import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.Question;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 10/3/15.
 */
@EFragment(R.layout.fragment_viewpager_selective_question)
public class SelectiveQuestionFragment extends Fragment {

    @ViewById
    TextView tv_answer;


    @ViewById
    TextView tv_no;

    @ViewById
    TextView tv_question;

    Question question;
    ShowAnswer mCallback;

    @AfterViews
    void init() {
        question = (Question) getArguments().getSerializable("question");
        if (question != null) {
//            tv_no.setText(question.q_no);
            tv_question.setText(question.question);
        }
        onAttachFragment(getParentFragment());

        if (question.selectedOption != 0) {
            mCallback.showClickedAnswer();

        }
    }

    @Click(R.id.show_answer)
    void showAnswer(){
        mCallback.showClickedAnswer();
    }


    public void onAttachFragment(Fragment fragment)
    {
        try
        {
            mCallback = (ShowAnswer) fragment;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(fragment.toString() + " must implement ShowAnswer");
        }
    }

   public interface ShowAnswer{
        void showClickedAnswer();
    }

}
