package com.example.akrmhrjn.sawarichalak.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;

public class AnkurButton extends Button {

    public String chooseFont;

    public AnkurButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fontawesome.ttf");
        setTypeface(font);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AnkurTextView);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.AnkurTextView_chooseFont:
                    chooseFont = a.getString(attr);
                    setText(chooseFont);
                    break;

            }
        }
        a.recycle();
    }
}
