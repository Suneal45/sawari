package com.example.akrmhrjn.sawarichalak.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.activities.ViewPagerActivity_;
import com.hannesdorfmann.annotatedadapter.annotation.ViewField;
import com.hannesdorfmann.annotatedadapter.annotation.ViewType;
import com.hannesdorfmann.annotatedadapter.support.recyclerview.SupportAnnotatedAdapter;

import java.util.List;


/**
 * Created by user on 9/26/15.
 */
public class RvSetsAdapter extends SupportAnnotatedAdapter implements RvSetsAdapterBinder {
    Context mContext;
    int i;

    @ViewType(
            layout = R.layout.grid_sets_list,
            views = {
                    @ViewField(id = R.id.tv_set_name, name = "setName", type = TextView.class)
            }
    )
    public final int mediumRow = 0;

    List<String> sets;

    public RvSetsAdapter(Context context, List<String> sets) {
        super(context);
        mContext = context;
        this.sets = sets;
    }

    @Override
    public int getItemCount() {
        return sets == null ? 0 : sets.size();
    }


    @Override
    public int getItemViewType(int position) {
        return mediumRow;
    }

    @Override
    public void bindViewHolder(RvSetsAdapterHolders.MediumRowViewHolder vh, final int position) {
        String title = sets.get(position);
        i = 1;
        //vh.setName.setText(title);
        vh.setName.setText(String.valueOf(i));
        i++;
        if (title.startsWith("sq")) {
            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewPagerActivity_.intent(mContext).extra("type", "sq").extra("position", position).start();
                }
            });
        } else if (title.startsWith("mcq")) {
            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewPagerActivity_.intent(mContext).extra("type", "mcq").extra("position", position).start();
                }
            });
        } else if (title.startsWith("misq")) { //TODO This might not be the right way to do this
            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewPagerActivity_.intent(mContext).extra("type", "misq").extra("position", position).start();
                }
            });
        } else if (title.startsWith("pq")) {
            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewPagerActivity_.intent(mContext).extra("type", "pic").extra("position", position).start();
                }
            });
        } else {
            //unknown type
        }

    }
}
