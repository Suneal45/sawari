package com.example.akrmhrjn.sawarichalak.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 2/27/16.
 */

public class User {

    @SerializedName("fb_unique_id")
    String fb_unique_id;

    @SerializedName("first_name")
    String first_name;

    @SerializedName("last_name")
    String last_name;

    @SerializedName("image")
    String image;

    public User(String fb_unique_id, String first_name, String last_name, String image ) {
        this.fb_unique_id = fb_unique_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.image = image;
    }
}