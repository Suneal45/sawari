package com.example.akrmhrjn.sawarichalak.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.fragments.QuestionSetsFragment_;


public class VpagerQueSetsAdapter extends FragmentPagerAdapter {

    String[] title;

    public VpagerQueSetsAdapter(FragmentManager manager, String[] title) {
        super(manager);
        this.title = title;
    }

    @Override
    public Fragment getItem(int position) {
        return QuestionSetsFragment_.newInstance(title[position]);
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
