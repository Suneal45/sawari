package com.example.akrmhrjn.sawarichalak.fragments;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.akrmhrjn.sawarichalak.R;
import com.example.akrmhrjn.sawarichalak.model.Question;
import com.kyleduo.switchbutton.SwitchButton;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 10/3/15.
 */
@EFragment(R.layout.fragment_viewpager_selective_answer)
public class SelectiveAnswerFragment extends Fragment {
    @ViewById
    TextView tv_answer;

    @ViewById
    SwitchButton know_answer_switch;

    @ViewById
    TextView tv_no;
    @ViewById
    TextView tv_question;
    @ViewById
    LinearLayout display_answer_settings;
    Question question;

    Answers mCallback;
/*

    @ViewById
    Button know_answer;

    @ViewById
    Button dont;
*/


    @AfterViews
    void init() {

        know_answer_switch.setThumbSize(42, 42);
        know_answer_switch.setThumbRadius(42);
        know_answer_switch.setThumbMargin(5, 5, 5, 5);
        know_answer_switch.setBackRadius(42);
        know_answer_switch.setBackColor(getResources().getColorStateList(R.color.purpleDark));
        know_answer_switch.setThumbColor(getResources().getColorStateList(R.color.WhiteSmoke));


        question = (Question) getArguments().getSerializable("question");
        if (question != null) {
//            tv_no.setText(question.q_no);
            tv_question.setText(question.question);
            tv_answer.setText(question.answer);
            if (question.selectedOption != 0) {
            /*    dont.setVisibility(View.GONE);
                know_answer.setVisibility(View.GONE);
*/
                display_answer_settings.setVisibility(View.GONE);

            }
        }
        know_answer_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (display_answer_settings.getVisibility() != View.GONE) {
                        mCallback.saveAnswer(1);
                    }
                    //     mCallback.saveOptionSelection(1);
                } else {
                    mCallback.saveAnswer(0);
                    //   mCallback.saveOptionSelection(1);
                }
            }
        });
        if (display_answer_settings.getVisibility() != View.GONE) {
            mCallback.saveOptionSelection(1);
        }
    }

    public interface Answers {
        void saveAnswer(int value);

        void saveOptionSelection(int position);
        //    void nextPage();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;
            try {
                mCallback = (Answers) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement Answers");
            }
        }
    }


}
